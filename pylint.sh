#!/bin/bash

set -e

pylint -f parseable "$@" > pylint.out || /bin/true
