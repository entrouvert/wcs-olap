import contextlib
from unittest import mock

import pytest

pytest.importorskip('sentry_sdk')


@contextlib.contextmanager
def capture_event_mock():
    import sentry_sdk

    capture_event_mock = mock.Mock()
    old_init = sentry_sdk.init

    def my_capture_event(*args, **kwargs):
        return capture_event_mock(*args, **kwargs)

    with contextlib.ExitStack() as stack:

        def new_init(*args, **kwargs):
            old_init(*args, **kwargs)
            if sentry_sdk.Hub.current.client:
                stack.enter_context(
                    mock.patch.object(
                        sentry_sdk.Hub.current.client.transport, 'capture_event', my_capture_event
                    )
                )

        stack.enter_context(mock.patch.object(sentry_sdk, 'init', new_init))
        yield capture_event_mock


@pytest.fixture
def sentry(olap_cmd):
    import sentry_sdk

    with olap_cmd.config() as config:
        if not config.has_section('sentry'):
            config.add_section('sentry')
        config.set('sentry', 'dsn', 'https://60460469613646a4a5efd61a6b239ce7@sentry.entrouvert.org/1')
        config.set('sentry', 'environment', 'prod')

    yield None

    # reset sentry_sdk configuration
    sentry_sdk.init()


def test_capture_event_on_feeder_error(mock_cursor_execute, sentry, olap_cmd, wcs, caplog):
    # set configuration value which make errors
    with olap_cmd.config() as config:
        config.set('wcs-olap', 'pg_dsn', '')
        config.set(wcs.url, 'schema', 'x' * 63)
    counter = 0

    def side_effect(*args, **kwargs):
        nonlocal counter
        if counter > 10:
            raise Exception('Boom!')
        counter += 1
        return mock.DEFAULT

    with capture_event_mock() as capture_event:
        with mock_cursor_execute(side_effect=side_effect):
            assert olap_cmd(no_log_errors=False) == 1
    assert capture_event.call_count == 1
    event = capture_event.call_args[0][0]
    assert len(event['breadcrumbs']) > 9
