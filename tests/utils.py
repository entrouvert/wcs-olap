import os
import subprocess
import sys

HOSTNAME = '127.0.0.1'
WCS_MANAGE = os.environ.get('WCS_MANAGE')


def run_wcs_script(wcs_dir, script, script_name):
    '''Run python script inside w.c.s. environment'''
    script_path = wcs_dir / (script_name + '.py')
    with script_path.open('w') as fd:
        fd.write(script)

    subprocess.check_call(
        [
            sys.executable,
            WCS_MANAGE,
            'runscript',
            '--app-dir',
            str(wcs_dir),
            '--vhost',
            HOSTNAME,
            str(script_path),
        ],
        env={'DJANGO_SETTINGS_MODULE': 'wcs.settings'},
    )
