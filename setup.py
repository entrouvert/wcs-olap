#! /usr/bin/env python

import os
import subprocess

from setuptools import find_packages, setup
from setuptools.command.sdist import sdist


class eo_sdist(sdist):
    def run(self):
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        with open('VERSION', 'w') as fd:
            fd.write(version)
        sdist.run(self)
        if os.path.exists('VERSION'):
            os.remove('VERSION')


def get_version():
    '''Use the VERSION, if absent generates a version with git describe, if not
    tag exists, take 0.0- and add the length of the commit log.
    '''
    if os.path.exists('VERSION'):
        with open('VERSION') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


setup(
    name='wcs-olap',
    version=get_version(),
    license='AGPLv3+',
    description='Export w.c.s. data to an OLAP cube',
    long_description=open('README.rst').read(),
    url='http://dev.entrouvert.org/projects/publik-bi/',
    author="Entr'ouvert",
    author_email='authentic@listes.entrouvert.com',
    maintainer='Benjamin Dauvergne',
    maintainer_email='bdauvergne@entrouvert.com',
    packages=find_packages(),
    include_package_data=True,
    install_requires=['requests', 'psycopg2', 'isodate', 'six', 'cached-property'],
    entry_points={
        'console_scripts': ['wcs-olap=wcs_olap.cmd:main'],
    },
    cmdclass={'sdist': eo_sdist},
)
