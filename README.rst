w.c.s. OLAP
~~~~~~~~~~~

Tool to export w.c.s. data in a database with star schema for making an OLAP
cube.

::

        usage: wcs-olap [--no-feed] [-a | --url URL] [-h] [--orig ORIG] [--key KEY]
                        [--pg-dsn PG_DSN] [--schema SCHEMA]
                        [config_path]

        Export W.C.S. data as a star schema in a postgresql DB

        positional arguments:
          config_path

        optional arguments:
          --no-feed        only produce the model
          -a, --all        synchronize all wcs
          --url URL        url of the w.c.s. instance
          -h, --help       show this help message and exit
          --orig ORIG      origin of the request for signatures
          --key KEY        HMAC key for signatures
          --pg-dsn PG_DSN  Psycopg2 DB DSN
          --schema SCHEMA  schema name

Requirements
------------

PostgreSQL > 9.4 is required.
